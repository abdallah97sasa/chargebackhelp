<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function (){
    return view('home');
});
Route::get('/index', [App\Http\Controllers\CashSourceController::class, 'index']);

Route::get('/cash', [App\Http\Controllers\CashSourceController::class, 'create']);
Route::post('/cash', [App\Http\Controllers\CashSourceController::class, 'store']);

Route::get('/credit_card', [App\Http\Controllers\CreditCardSourceController::class, 'create']);
Route::post('/credit_card', [App\Http\Controllers\CreditCardSourceController::class, 'store']);

Route::get('/bank_transfer', [App\Http\Controllers\BankTransferController::class, 'create']);
Route::post('/bank_transfer', [App\Http\Controllers\BankTransferController::class, 'store']);

