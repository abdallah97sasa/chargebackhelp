<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface  TransactionInterface
{
    /**
     * Validate Inputs
     */
    public function validate();
    /**
     * Return total amount
     */
    public function amount();
    /**
     * Return Inputs
     */
    public function inputs();
}
