<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankTransfer extends Model implements TransactionInterface
{
    use HasFactory;
    protected $fillable = [
        'transfer_date',
        'customer_name',
        'account_number',
        'amount',
        'hundreds',
    ];
    public function validate()
    {
        // TODO: Implement validate() method.
    }

    public function amount()
    {
        // TODO: Implement amount() method.
    }

    public function inputs()
    {
        // TODO: Implement inputs() method.
    }
}
