<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashSource extends Model implements TransactionInterface
{
    use HasFactory;
    protected $fillable = [
        'ones',
        'fives',
        'tens',
        'fifties',
        'hundreds',
        'total',
    ];
    public function validate()
    {
        $rules= [
            'ones' => 'required|integer|min:0|max:10000',
            'fives' => 'required|integer|min:0|max:2000',
            'tens' => 'required|integer|min:0|max:1000',
            'fifties' => 'required|integer|min:0|max:200',
            'hundreds' => 'required|integer|min:0|max:100',
            'total' => 'required|integer|min:0|max:10000',
        ];
    }

    public function amount()
    {
        return $this->total;
    }

    public function inputs()
    {
        // TODO: Implement inputs() method.
    }
}
