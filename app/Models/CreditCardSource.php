<?php

namespace App\Models;

use App\Rules\DateRule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreditCardSource extends Model implements TransactionInterface
{
    use HasFactory;
    protected $fillable = [
        'card_number',
        'expiration_date',
        'cardholder',
        'cvv',
        'amount',
    ];
    public function validate()
    {
        return [
            'cardholder' => 'required|string|max:100',
            'card_number' => 'required|string|size:16|starts_with:4',
            'expiration_date' => ['required',new DateRule()],
            'cvv' => 'required|string|size:3',
            'amount' => 'required|integer|min:0|max:20000',
        ];
    }

    public function amount()
    {
        return $this->amount;
    }

    public function inputs()
    {
        // TODO: Implement inputs() method.
    }
}
