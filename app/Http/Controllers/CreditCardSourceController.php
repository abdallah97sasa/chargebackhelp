<?php

namespace App\Http\Controllers;


use App\Models\CreditCardSource;
use App\Rules\DateRule;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CreditCardSourceController extends Controller
{
    private $model_instance;
    public function __construct()
    {
        $this->model_instance = CreditCardSource::class;
    }
    public function index()
    {
        return view('index');
    }
    public function create()
    {
        return view('creditCard/create');
    }
    private function ValidationRules()
    {
        return [
            'cardholder' => 'required|string|max:100',
            'card_number' => 'required|string|size:16|starts_with:4',
            'expiration_date' => ['required','string',new DateRule()],
            'cvv' => 'required|string|size:3',
            'amount' => 'required|integer|min:1|max:20000',
        ];
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->ValidationRules());



        if ($validator->fails()) {
            return redirect('/credit_card') ->withErrors($validator) ->withInput();
        }
        try {
            $validated_data = $validator->validated();
            $object = $this->model_instance::create($validated_data);
            return redirect('/index')->with('Success', 'Transaction ID #'.$object->id.' ,Total : '.$request->amount)
                ->with('inputs',"  cardholder: ".$request->cardholder .' ,card_number: '.$request->card_number .'
                ,expiration_date: '.$request->expiration_date.' ,cvv: '.$request->cvv.
                    ' ,amount: '.$request->amount);

        } catch (\Error $ex) {
            return redirect('/credit_card')->with('errors', 'Something went wrong');
        }

    }
}
