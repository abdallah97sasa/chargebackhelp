<?php

namespace App\Http\Controllers;


use App\Models\CashSource;
use App\Models\TransactionInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CashSourceController extends Controller
{
    private $model_instance;
    public function __construct()
    {
        $this->model_instance = CashSource::class;
    }
    public function index()
    {
        return view('index');
    }
    public function create()
    {
        return view('cash/create');
    }
    private function ValidationRules()
    {
        return [
            'ones' => 'required|integer|min:0|max:10000',
            'fives' => 'required|integer|min:0|max:2000',
            'tens' => 'required|integer|min:0|max:1000',
            'fifties' => 'required|integer|min:0|max:200',
            'hundreds' => 'required|integer|min:0|max:100',
            'total' => 'required|integer|min:1|max:10000',
        ];
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->ValidationRules());
        if ($validator->fails()) {
            return redirect('/cash') ->withErrors($validator) ->withInput();
        }
        try {
            $validated_data = $validator->validated();

            $object = $this->model_instance::create($validated_data);
            return redirect('/index')->with('Success', 'Transaction ID #'.$object->id.' ,Total : '.$request->total)
                ->with('inputs',"  Ones: ".$request->ones .' ,Fives: '.$request->fives .'
                ,Tens: '.$request->tens.' ,Fifties: '.$request->fifties.
                    ' ,Hundreds: '.$request->hundreds);

        } catch (\Error $ex) {
            return redirect('/cash')->with('errors', 'Something went wrong');
        }

    }
}
