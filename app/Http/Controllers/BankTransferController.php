<?php

namespace App\Http\Controllers;


use App\Models\BankTransfer;
use App\Rules\BankDateRule;
use App\Rules\DateRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BankTransferController extends Controller
{
    private $model_instance;
    public function __construct()
    {
        $this->model_instance = BankTransfer::class;
    }
    public function index()
    {
        return view('index');
    }
    public function create()
    {
        return view('bankTransfer/create');
    }
    private function ValidationRules()
    {
        return [
            'transfer_date' => ['required','string',new BankDateRule()],
            'customer_name' => 'required|string|max:100',
            'account_number' => 'required|string|size:6',
            'amount' => 'required|integer|min:1|max:20000',
        ];
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->ValidationRules());



        if ($validator->fails()) {
            return redirect('/bank_transfer') ->withErrors($validator) ->withInput();
        }
        try {
            $validated_data = $validator->validated();
            $object = $this->model_instance::create($validated_data);
            return redirect('/index')->with('Success', 'Transaction ID #'.$object->id.' ,Total : '.$request->amount)
                ->with('inputs',"  transfer_date: ".$request->transfer_date .' ,customer_name: '.$request->customer_name .'
                ,account_number: '.$request->account_number. ' ,amount: '.$request->amount);

        } catch (\Error $ex) {
            return redirect('/bank_transfer')->with('errors', 'Something went wrong');
        }

    }
}
