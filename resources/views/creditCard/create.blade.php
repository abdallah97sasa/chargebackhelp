@extends('layouts.app')

@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)

            <h2 class="alert-danger">{{ $error }}</h2>

        @endforeach
    @endif
    <main>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="credit_card" role="tabpanel" aria-labelledby="credit_card-tab">
                <div class="col-lg-12 mx-auto p-1 py-md-3 ">
                    <form action="{{ action('CreditCardSourceController@store') }}" method="POST">
                        @csrf
                        <div class="padding">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Credit Card</strong>
                                            <small>enter your card details</small>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="cardholder">Name</label>
                                                        <input class="form-control" id="cardholder" name="cardholder" type="text"
                                                               placeholder="Enter your name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="card_number">Credit Card Number</label>
                                                        <div class="input-group">
                                                            <input class="form-control" type="text"
                                                                   placeholder="0000 0000 0000 0000"
                                                                   autocomplete="credit_card_number"
                                                                   name="card_number">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <label for="month">Month</label>
                                                    <select class="form-control" id="month" name="month">
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                        <option>7</option>
                                                        <option>8</option>
                                                        <option>9</option>
                                                        <option>10</option>
                                                        <option>11</option>
                                                        <option>12</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="year">Year</label>
                                                    <input type="text" class="form-control" name="expiration_date" id="datepicker">
<script>
    $("#datepicker").datepicker( {
        format: "mm-yyyy",
        minViewMode: "months",
    });
</script>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="cvv">CVV/CVC</label>
                                                        <input class="form-control" id="cvv" name="cvv" type="text"
                                                               placeholder="123">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="amount">Amount</label>
                                                        <input class="form-control" id="amount" name="amount" type="number"
                                                               placeholder="123">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button class="btn btn-sm btn-success float-right" type="submit">
                                                <i class="mdi mdi-gamepad-circle"></i> Pay
                                            </button>
                                            <button class="btn btn-sm btn-danger" type="reset">
                                                <i class="mdi mdi-lock-reset"></i> Reset
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </main>
@endsection
