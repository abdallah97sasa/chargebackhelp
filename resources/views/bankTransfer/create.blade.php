@extends('layouts.app')

@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)

            <h2 class="alert-danger">{{ $error }}</h2>

        @endforeach
    @endif
    <main>



        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="bank_transfers" role="tabpanel" aria-labelledby="bank_transfers-tab">
                <div class="col-lg-12 mx-auto p-1 py-md-3 ">
                    <form form action="{{ action('BankTransferController@store') }}" method="POST">
                        @csrf
                        <div class="padding">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Bank Transfers</strong>
                                            <small>enter your details</small>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="ones">Transfer Date</label>
                                                    <input class="form-control" id="transfer_date" name="transfer_date"
                                                           type="date"
                                                           placeholder="2022/4/20">
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <label for="customer_name">Customer name</label>
                                                    <input type="text" name="customer_name" class="form-control"
                                                           id="customer_name"
                                                           placeholder="jon kara">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="account_number">Account number</label>
                                                        <input type="text" name="account_number" class="form-control"
                                                               id="account_number" placeholder="TR1025">
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="amount">Amount</label>
                                                        <input type="number" max="20000" name="amount"
                                                               class="form-control"
                                                               id="amount">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button class="btn btn-sm btn-success float-right" type="submit">
                                            <i class="mdi mdi-gamepad-circle"></i> Pay
                                        </button>
                                        <button class="btn btn-sm btn-danger" type="reset">
                                            <i class="mdi mdi-lock-reset"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </main>
@endsection
