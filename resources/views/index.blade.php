@extends('layouts.app')

@section('content')
    <div class="col-lg-8 mx-auto p-3 py-md-5">
@if(session()->has('Success'))
        <h1>  {{ (session()->get('Success'))}}</h1>
@endif
        <br>
    @if(session()->has('inputs'))
        <h3>  {{ (session()->get('inputs'))}}</h3>
    @endif

    </div>
</div>
@endsection
