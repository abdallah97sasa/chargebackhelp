@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

                <ul class="navbar-nav ms-auto">

                    <li class="nav-item">
                        <a class="nav-link" href="/cash">Pay With Cash</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/credit_card">Pay With Credit Card</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="/bank_transfer">Pay With Bank Transfer</a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>
@endsection
