@extends('layouts.app')

@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)

            <h2 class="alert-danger">{{ $error }}</h2>

        @endforeach
    @endif
    <main>



        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="cash" role="tabpanel" aria-labelledby="cash-tab">
                <div class="col-lg-12 mx-auto p-1 py-md-3 ">
                    <form action="{{ action('CashSourceController@store') }}" method="POST">
                        @csrf
                        <div class="padding">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Cash</strong>
                                            <small>enter the amount</small>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">

                                                <div class="form-group col-sm-4">
                                                    <label for="ones">Ones</label>
                                                    <input onchange="validation()" max="10000" class="form-control"
                                                           id="ones" value="0" name="ones" type="number" min="0"
                                                           required>
                                                </div>

                                                <div class="form-group col-sm-4">
                                                    <label for="fives">Fives</label>
                                                    <input onchange="validation()" type="number" max="2000" value="0"
                                                           name="fives" class="form-control" id="fives" min="0"
                                                           required>
                                                </div>

                                                <div class="col-sm-4 form-group">
                                                    <label for="tens">Tens</label>
                                                    <input onchange="validation()" type="number" max="1000" value="0"
                                                           name="tens" class="form-control" id="tens" min="0" required>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4 form-group">
                                                    <label for="fifties">Fifties</label>
                                                    <input onchange="validation()" type="number" max="200" value="0"
                                                           name="fifties" class="form-control" id="fifties" min="0"
                                                           required>
                                                </div>

                                                <div class="col-sm-4 form-group">
                                                    <label for="hundreds">Hundreds</label>
                                                    <input onchange="validation()" type="number" max="100" value="0"
                                                           name="hundreds" class="form-control" id="hundreds" min="0"
                                                           required>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 form-group">
                                                <label for="hundreds">Total</label>
                                                <input type="number" max="10000" name="total" id="total" min="0"
                                                       required readonly class="form-control">
                                            </div>
                                            <script>
                                                function validation() {
                                                    ones = parseInt(document.getElementById('ones').value);
                                                    fives = parseInt(document.getElementById('fives').value);
                                                    tens = parseInt(document.getElementById('tens').value);
                                                    fifties = parseInt(document.getElementById('fifties').value);
                                                    hundreds = parseInt(document.getElementById('hundreds').value);

                                                    total = parseInt(ones + (fives * 5) + (tens * 10) + (fifties * 50) + (hundreds * 100) + 0);
                                                    console.log(total);
                                                    document.getElementById('total').value = total;
                                                }
                                            </script>
                                        </div>
                                        <div class="card-footer">
                                            <button class="btn btn-sm btn-success float-right" type="submit">
                                                <i class="mdi mdi-gamepad-circle"></i> Pay
                                            </button>
                                            <button class="btn btn-sm btn-danger" type="reset">
                                                <i class="mdi mdi-lock-reset"></i> Reset
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </main>
@endsection
