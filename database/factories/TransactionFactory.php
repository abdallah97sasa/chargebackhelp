<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'card_number' => '4321432143214321',
            'expiration_date' => '06/2022',
            'cardholder' => Str::random(10),
            'cvv' => '123',
            'amount' => rand(1,20000),
        ];
    }
}
